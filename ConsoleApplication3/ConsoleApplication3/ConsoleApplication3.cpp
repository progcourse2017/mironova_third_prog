// ConsoleApplication3.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>
#include <ctime>
using namespace std;

int main()
{
	srand(time(0)); 
	int **array = new int*[2]; 
	for (int i = 0; i< 2; i++)
		array[i] = new int[10];
	for (int row = 0; row < 2; row++)
		for (int column = 0; column < 10; column++)
			array[row][column] = (rand() % 100 + 1) / int((rand() % 100 + 1)); 
	for (int row = 0; row < 2; row++)
	{
		for (int column = 0; column < 10; column++)
			cout  << array[row][column] << "   ";
		cout << endl;
	}
	
	for (int i = 0; i < 2; i++)
		delete[]array[i];
	system("pause");
	return 0;
}